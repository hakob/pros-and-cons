import React, {Component} from 'react';
import List from './List';
import Modal from 'simple-react-modal';

class Home extends Component {
    constructor(props) {
        super(props);

        this.username = this.props.username;
        this.userId = null;
        this.groupId = null;
        this.state = {
            pros: [],
            cons: []
        };
    }

    showModal(text) {
        this.setState({show: true, text})
    }

    closeModal() {
        this.setState({show: false})
    }

    async fetchUserData() {
        let response = await fetch(`https://avetiq-test.firebaseapp.com/user/${this.username}`);
        let data = await response.json();
        this.userId = data['userId'];

        response = await fetch(`https://avetiq-test.firebaseapp.com/group/${this.username}`);
        data = await response.json();
        this.groupId = data['groupId'];
    }

    async fetchData() {
        let response = await fetch(`https://avetiq-test.firebaseapp.com/proscons/group/${this.groupId}/user/${this.userId}`);
        return await response.json();
    }

    componentDidMount() {
        this.showModal("Loading");
        this.fetchUserData.call(this).then(this.fetchData.bind(this)).then(data => {
            this.setState({
                pros: data.pros,
                cons: data.cons
            });

            this.closeModal();
        });
    }

    onProsChange(items) {
        this.setState({pros: items}, () => {
            this.onDataChange().then(console.log)
        });
    }

    onConsChange(items) {
        this.setState({cons: items}, () => {
            this.onDataChange().then(console.log)
        });
    }

    async onDataChange() {
        this.showModal("Saving");
        let response = await fetch(`https://avetiq-test.firebaseapp.com/proscons/group/${this.groupId}/user/${this.userId}`, {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(this.state)
        });

        let data = await response.json();
        this.closeModal();

        return data;
    }

    render() {
        return (
            <div>
                <div className="main-header">Should I ... ?</div>
                <div className="row">
                    <List items={this.state.pros} name="Pros" onChange={this.onProsChange.bind(this)}/>
                    <List items={this.state.cons} name="Cons" onChange={this.onConsChange.bind(this)}/>
                </div>
                <Modal show={this.state.show} onClose={this.closeModal.bind(this)}>
                    <p>{this.state.text}...</p>
                </Modal>
            </div>
        )
    }
}

export default Home
