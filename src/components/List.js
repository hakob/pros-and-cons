import React, {Component} from 'react';
import Modal from 'simple-react-modal';

class List extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [...this.props.items]
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            items: [...nextProps.items]
        })
    }

    showModal(index) {
        this.setState({show: true, updateIndex: index, updateInput: this.state.items[index]})
    }

    closeModal() {
        this.setState({show: false, updateIndex: undefined})
    }

    handleAddChange(e) {
        this.setState({input: e.target.value});
    }

    handleAddClick() {
        if (!this.state.input) {
            return;
        }

        if (this.state.items.includes(this.state.input)) {
            alert("Already in list");
            return;
        }

        this.state.items.push(this.state.input);

        this.setState({items: this.state.items, input: ''});
        this.props.onChange([...this.state.items]);
    }

    handleRemoveClick(index) {
        this.state.items.splice(index, 1);

        this.setState({items: this.state.items});
        this.props.onChange([...this.state.items]);
    }

    handleUpdateChange(e) {
        this.setState({updateInput: e.target.value});
    }

    handleUpdateClick() {
        if (!this.state.updateInput) {
            return;
        }

        if (this.state.items.includes(this.state.input)) {
            alert("Already in list");
            return;
        }

        let items = this.state.items;
        items[this.state.updateIndex] = this.state.updateInput;

        this.setState({items: this.state.items, updateInput: ''});
        this.closeModal();
        this.props.onChange([...this.state.items]);
    }


    render() {
        return (
            <div className="column list">
                <div className="list-header">{this.props.name}</div>
                <ol>
                    {this.state.items.map((i, index) =>
                        <li key={index}><span onDoubleClick={this.showModal.bind(this, index)}>{i}</span>
                            <button className="circle-button" onClick={this.handleRemoveClick.bind(this, index)}>-
                            </button>
                        </li>
                    )}
                </ol>

                <div className="list-add">
                    <input placeholder="New..." type="text" value={this.state.input}
                           onChange={this.handleAddChange.bind(this)}/>
                    <button className="circle-button" onClick={this.handleAddClick.bind(this)}>+</button>
                </div>
                <Modal show={this.state.show} onClose={this.closeModal.bind(this)}>
                    <div>
                        <p>Update item value</p>
                        <input className="margin5" type="text" value={this.state.updateInput}
                               onChange={this.handleUpdateChange.bind(this)}/>
                        <button onClick={this.handleUpdateClick.bind(this)}>Update</button>
                    </div>
                </Modal>
            </div>
        )
    }
}

export default List
